## Устанавливаемые сервисы: 00000

1. Jbpm
2. Nginx
3. Postgres
4. Prometheus
5. Grafana
6. Node-exporter

## Подготовка ОС:

1. Настройка hosts:
```
echo "127.0.0.1 jbpm" >> /etc/hosts
echo "127.0.0.1 prometheus" >> /etc/hosts
echo "127.0.0.1 grafana" >> /etc/hosts
```

2. Создаем папку куда будем скачивать проект и переходим в нее:

```
mkdir newfolder
cd newfolder
```

3. Скачиваем проект:

```
git clone https://gitlab.com/legoti4/homework1.git
```
4. Переходим в папку с файлом docker-compose.yml

```
cd homework1
```

5. Запускаем проект:

```
docker-compose up -d
```

## Проверка доступа к сервисам:

 Сервисы достцпны по следующим адресам:

2.1. http://grafana    
```
Логин: admin    
Пароль: Qwerty123    
```
2.2. http://prometheus    
2.3. http://jbpm

Параметры доступа к BD: Postgres прописаны в файле docker-compose.yml

